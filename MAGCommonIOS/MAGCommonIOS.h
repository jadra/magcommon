//
//  MAGCommonIOS.h
//  MAGCommonIOS
//
//  Created by john on 6/12/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MAGCommonIOS.
FOUNDATION_EXPORT double MAGCommonIOSVersionNumber;

//! Project version string for MAGCommonIOS.
FOUNDATION_EXPORT const unsigned char MAGCommonIOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MAGCommonIOS/PublicHeader.h>

#import <MAGCommonIOS/MAGFormat.h>
#import <MAGCommonIOS/MAGInfo.h>
#import <MAGCommonIOS/MAGLink.h>
#import <MAGCommonIOS/MAGThumbnailCreator.h>
#import <MAGCommonIOS/MAGWriter.h>
#import <MAGCommonIOS/MAGReader.h>
#import <MAGCommonIOS/MAGPage.h>

