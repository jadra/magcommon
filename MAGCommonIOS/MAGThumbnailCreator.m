//
//  MAGThumbnailCreator.m
//  MAGCommonIOS
//
//  Created by john on 6/12/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGThumbnailCreator.h"
#import <UIKit/UIKit.h>

@implementation MAGThumbnailCreator
+ (NSData *)dataFromCIImage:(CIImage *)image {
    return UIImageJPEGRepresentation([UIImage imageWithCIImage:image], 0.75);
}
@end
