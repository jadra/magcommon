//
//  MAGThumbnailCreator.h
//  MAGCommonIOS
//
//  Created by john on 6/12/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAGWriter.h"

@interface MAGThumbnailCreator : NSObject<MAGThumbnailCreatable>

@end
