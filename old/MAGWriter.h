//
//  MagWriter.h
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>



/**
 The MAGWriter creates mag and thumb files from input jpegs and a json file that should
 be located in the same directory as the input jpegs.
 */
@interface MAGWriter : NSObject

/** The default mag file name if none is provided. */
@property (nonnull, nonatomic, readonly) NSString *defaultMagFileName;

/** The default thumb file name if none is provided. */
@property (nonnull, nonatomic, readonly) NSString *defaultThumbFileName;

/** The mag file name to provide, if any. */
@property (nullable, nonatomic) NSString *magFileName;

/** The thumb file name to provide, if any. */
@property (nullable, nonatomic) NSString *thumbFileName;


/**
 Creates a MAGWriter instance from provided parameters.
 @param inputPath The path where jpegs and info.json files can be found.
 @param outputPath The path where the mag and thumbs file are generated to.
 @return MAGWriter
 */

- (__nonnull instancetype)initWithInputPath:(NSString * __nullable)inputPath outputPath:(NSString * __nullable)outputPath;


/**
 Generate mag and thumb files
 @param error A pointer to an error object to provide that will be filled with an error if one occurs.
 @return YES if generation succeeds, NO if not.
 */

- (BOOL)processFilesWithError:(NSError **)error;

@end
