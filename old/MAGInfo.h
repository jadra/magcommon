//
//  MAGInfo.h
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
The MAGInfo class contains information about a mag file.
 */
@interface MAGInfo : NSObject

/** The version of the mag file. */
@property (nonatomic) NSNumber *version;

/** The count of images in the mag file. */
@property (nonatomic) NSNumber *pageCount;

/** The width of the images.*/
@property (nonatomic) NSNumber *width;

/** The height of the images. */
@property (nonatomic) NSNumber *height;

/** The name of the magazine. */
@property (nonatomic) NSString *name;

/** The issue (typically a date string). */
@property (nonatomic) NSString *issue;

/** The name of the publisher. */
@property (nonatomic) NSString *publisher;

/** A dictionary of link entries. */
@property (nonatomic) NSDictionary  *links;


/**
 Create a MAGInfo instance from the provided dictionary.
 @param dictionary A dictionary containing information to instance a MAGInfo object.
 @return MAGInfo instance.
 */

+ (MAGInfo *)infoFromDictionary:(NSDictionary *)dictionary;

@end
