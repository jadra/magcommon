//
//  main.m
//  MagMaker
//
//  Created by john on 5/23/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAGWriter.h"


NSString * const ReaderFilesIn = @"ReaderFilesIn";
NSString * const ReaderFileOut = @"ReaderFileOut";


int main(int argc, const char * argv[]) {
    
    if (argc < 3) {
        NSLog(@"\nUsage: MagMaker input_dir output_dir\n");
        return EXIT_FAILURE;
    }
    
    if (argv[1] == NULL || argv[2] == NULL) {
        NSLog(@"\nUsage: MagMaker input_dir output_dir\n");
        return EXIT_FAILURE;
    }
    
    NSString *inputPath = [NSString stringWithUTF8String:argv[1]];
    NSString *outputPath  = [NSString stringWithUTF8String:argv[2]];
    
    @autoreleasepool {
                
        NSError *error;
        MAGWriter *writer = [[MAGWriter alloc] initWithInputPath:inputPath outputPath:outputPath];
        if ([writer processFilesWithError:&error]) {
            NSLog(@"Creation finished successfully!");
        }
        else {
            NSLog(@"Creation failed with error\n%@", [error localizedDescription]);
        }
        
    }
    return 0;
}
