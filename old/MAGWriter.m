//
//  MAGWriter.m
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import "MAGWriter.h"
#import "MAGFormat.h"
#import "MAGInfo.h"
#import "MAGLink.h"
#import <AppKit/AppKit.h>

NSString * const MAG_INFO_FILENAME = @"info.json";
NSString * const MAG_JPEG = @"jpg";
NSString * const MAG_TEMP_THUMB_FORMAT = @"%d.jpg";
NSString * const MAG_THUMB_FILTER_PREDICATE = @"SELF endswith[c] 'jpg'";

@interface MAGWriter()
@property (nonatomic) NSString *inputPath;
@property (nonatomic) NSString *outputPath;
@end

@implementation MAGWriter

- (__nonnull instancetype)initWithInputPath:(NSString * __nullable)inputPath outputPath:(NSString * __nullable)outputPath {
    self = [super init];
    if (self) {
        self.inputPath = inputPath;
        self.outputPath = outputPath;
    }
    return self;
}

- (NSString * __nonnull)defaultMagFileName {
    return @"out.mag";
}

- (NSString * __nonnull)defaultThumbFileName {
    return @"out.thumb";
}

- (BOOL)processFilesWithError:(NSError **)error {
    
    NSFileManager *fileManager = [NSFileManager new];
    
    // Our directory has no subdirectories (to keep it simple) so the following is sufficient
    NSArray *dirContents =  [fileManager contentsOfDirectoryAtPath:_inputPath error:error];
    
    if (*error) {
        return NO;
    }
    
    if ([dirContents count] == 0) {
        
       *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError userInfo:@{NSLocalizedDescriptionKey : @"Directory is empty."}];
        return NO;
    }
    
    NSString *infoFile = [_inputPath stringByAppendingPathComponent:MAG_INFO_FILENAME];
    
    if (![fileManager fileExistsAtPath:infoFile isDirectory:NULL]) {
        *error = [NSError errorWithDomain:NSCocoaErrorDomain
                                  code:NSFileNoSuchFileError
                              userInfo:@{NSLocalizedDescriptionKey : [NSString stringWithFormat:@"Could not find info.json file at %@.", _inputPath]}];
        return NO;
    }
    
    
    NSString *magFilePath = [_outputPath stringByAppendingPathComponent: !_magFileName ? self.defaultMagFileName : _magFileName];
    
    // We will ignore FileExistsError but other errors will be handled
    if(![fileManager createDirectoryAtPath:_outputPath withIntermediateDirectories:NO attributes:nil error:error]) {
        if ([*error code] != NSFileWriteFileExistsError) {
            return NO;
        }
        else {
            *error = nil;
        }
    }
    
    [fileManager createFileAtPath:magFilePath contents:nil attributes:nil];
    
    MAGInfo *magInfo = [self extractInfo:infoFile error: error];
    if (*error) {
        return NO;
    }
    
    MAGFormatHeader header = [self headerFromMagInfo:magInfo];

    
    NSUInteger totalJPEGLength = 0;
    NSFileHandle *magFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:magFilePath];
    
    if (magFileHandle) {

        [magFileHandle writeData:[NSData dataWithBytes:&header length:sizeof(MAGFormatHeader)]];
        
        NSArray *sortedDirContents = [dirContents sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return obj1 > obj2;
        }];
        
        NSUInteger currentOffset = [magFileHandle offsetInFile];
        
        int idx = 0;
        for (NSString *fileName in sortedDirContents) {
            if ([fileName hasSuffix:MAG_JPEG]) {
                @autoreleasepool {
                    NSString *filePath = [_inputPath stringByAppendingPathComponent:fileName];
                    NSData *picData =  [NSData dataWithContentsOfFile:filePath];
                    
                    
                    NSData *thumb = [self thumbnailFromPicData:picData];
                    [thumb writeToFile:[_outputPath stringByAppendingPathComponent:[NSString stringWithFormat:MAG_TEMP_THUMB_FORMAT, idx]] atomically:NO];
                    
                    
                    NSUInteger picDataLength = [picData length];
                    MAGPicture picture = {.offset = currentOffset, .size = picDataLength};
                    header.picJumpTable[idx] = picture;
                    [magFileHandle writeData:picData];
                    currentOffset += picDataLength;
                    totalJPEGLength += picDataLength;
                    idx++;
                }
            }
        }
        
        header.picsSize = totalJPEGLength;
        header.linksOffset = currentOffset;
        NSData *linksData = [NSJSONSerialization dataWithJSONObject:magInfo.links options:0 error:error];
        
        if (!*error) {
            [magFileHandle writeData:linksData];
            header.linksSize = [linksData length];
        }
        
        [magFileHandle seekToFileOffset:sizeof(long) * 4];
        [magFileHandle writeData:[NSData dataWithBytes:&header.linksOffset length:sizeof(long)]];
        [magFileHandle writeData:[NSData dataWithBytes:&header.linksSize length:sizeof(long)]];
        [magFileHandle writeData:[NSData dataWithBytes:&header.picsOffset length:sizeof(long)]];
        [magFileHandle writeData:[NSData dataWithBytes:&header.picsSize length:sizeof(long)]];
        [magFileHandle seekToFileOffset:MAGFORMAT_METADATA_SIZE];
        [magFileHandle writeData:[NSData dataWithBytes:&header.picJumpTable length:sizeof(header.picJumpTable)]];
        
    }
    else {
        *error = [NSError
                  errorWithDomain:NSCocoaErrorDomain
                  code:NSFileNoSuchFileError
                  userInfo: @{
                    NSLocalizedDescriptionKey : [NSString stringWithFormat:@"Could not open file at %@.", magFilePath]
                }];
        
        return NO;
    }
    
    [magFileHandle closeFile];
    
    
    if (![self createThumbFile:error]) {
        return NO;
    }
    
    return YES;
}


- (MAGInfo *)extractInfo:(NSString *)infoFile error:(NSError **)error {
    NSDictionary *info = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:infoFile] options:0 error:error];
    
    if (!info) {
        return nil;
    }
    
    return [MAGInfo infoFromDictionary:info];
}

- (MAGFormatHeader)headerFromMagInfo:(MAGInfo *)magInfo {
    MAGFormatHeader header;
    
    header.version = [magInfo.version longValue];
    header.pageCount = [magInfo.pageCount longValue];
    header.width = [magInfo.width longValue];
    header.height = [magInfo.height longValue];
    header.linksSize = 0;
    header.linksOffset = 0;
    header.picsSize = 0;
    header.picsOffset = sizeof(MAGFormatHeader);
    strcpy(header.name, [magInfo.name UTF8String]);
    strcpy(header.issue, [magInfo.issue UTF8String]);
    strcpy(header.publisher, [magInfo.publisher UTF8String]);
    
    for (int i = 0; i < header.pageCount; i++) {
        MAGPicture picture = {.offset = 0, .size = 0};
        header.picJumpTable[i] = picture;
    }
    
    return header;
}


- (NSData *)thumbnailFromPicData:(NSData *)picData {
    
    CIImage *image = [CIImage imageWithData:picData];
    CIFilter *filter = [CIFilter filterWithName:@"CILanczosScaleTransform"];
    [filter setValue:image forKey:kCIInputImageKey];
    [filter setValue:@(0.0625) forKey:kCIInputScaleKey];

    CIImage *scaledImage = filter.outputImage;
    NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc] initWithCIImage:scaledImage];
    NSData *thumbNailData = [imageRep representationUsingType:NSJPEGFileType properties:@{}];
    
    return thumbNailData;
}

- (BOOL)createThumbFile:(NSError **)error {
    
    NSArray *dirContents =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_outputPath error:error];
    
    if (*error) {
        return NO;
    }
    
    NSArray *sortedThumbs = [dirContents sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return obj1 > obj2;
    }];
    
    NSArray *filteredThumbs = [sortedThumbs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:MAG_THUMB_FILTER_PREDICATE]];
    
    NSString *magThumbFileName = [_outputPath stringByAppendingPathComponent:!_thumbFileName ? self.defaultThumbFileName : _thumbFileName];
    
    [[NSFileManager defaultManager] createFileAtPath:magThumbFileName contents:nil attributes:nil];
    
    NSFileHandle *thumbFileHandle = [NSFileHandle fileHandleForWritingAtPath:magThumbFileName];
    
    if (thumbFileHandle) {
        int idx = 0;
        NSUInteger currentOffset = sizeof(MAGThumbFormatHeader);
        MAGThumbFormatHeader header;
        header.pageCount = [filteredThumbs count];
        [thumbFileHandle writeData:[NSData dataWithBytes:&header length:sizeof(MAGThumbFormatHeader)]];
        
        for (NSString *thumbFileName in filteredThumbs) {
            NSData *thumbData = [NSData dataWithContentsOfFile:[_outputPath stringByAppendingPathComponent:thumbFileName]];
            MAGPicture picture = {.offset = currentOffset, .size = [thumbData length]};
            header.thumbJumpTable[idx] = picture;
            [thumbFileHandle writeData:thumbData];
            currentOffset += [thumbData length];
            idx++;
        }
        
        [thumbFileHandle seekToFileOffset:sizeof(long)];
        
        for (int i = 0; i < idx; i++) {
            [thumbFileHandle writeData:[NSData dataWithBytes:&header.thumbJumpTable[i] length:sizeof(MAGPicture)]];
        }
    }
    else {
        *error = [NSError errorWithDomain:@"com.magcreator" code:1 userInfo:@{NSLocalizedDescriptionKey : @"Could not create 'out.thumb' file!"}];
        return NO;
    }
    
    
    [thumbFileHandle closeFile];
    
    //Clean-up
    for (NSString *thumb in filteredThumbs) {
        [[NSFileManager defaultManager] removeItemAtPath:[_outputPath stringByAppendingPathComponent:thumb] error:nil];
    }

    return YES;
}

@end
