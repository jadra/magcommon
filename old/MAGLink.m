//
//  MAGLink.m
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGLink.h"

@implementation MAGLink

+ (MAGLink *)linkFromDictionary:(NSDictionary *)dictionary {
    MAGLink *magLink = [MAGLink new];
    magLink.identifier = [[NSUUID UUID] UUIDString];
    
    for (NSString *key in [dictionary keyEnumerator]) {
        if ([key isEqualToString:@"type"]) {
            magLink.type = [dictionary[@"type"] integerValue];
        }
        else if ([key isEqualToString:@"dimensions"]) {
            magLink.dimensions = dictionary[@"dimensions"];
        }
        else if ([key isEqualToString:@"href"]) {
            magLink.href = dictionary[@"href"];
        }
        else if ([key isEqualToString:@"linkTo"]) {
            magLink.linkTo = dictionary[@"linkTo"];
        }
    }
    
    return magLink;
}

- (CGRect)rectDimensions {
    if (_dimensions) {
        NSNumber *x = _dimensions[@"x"];
        NSNumber *y = _dimensions[@"y"];
        NSNumber *width = _dimensions[@"width"];
        NSNumber *height = _dimensions[@"height"];
        if (x && y && width && height) {
            return CGRectMake([x floatValue], [y floatValue], [width floatValue], [height floatValue]);
        }
    }
    return CGRectZero;
}

@end
