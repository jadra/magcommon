//
//  MAGWriterTests.swift
//  MagMakerTests
//
//  Created by john on 5/24/18.
//  Copyright © 2018 john. All rights reserved.
//

import XCTest

class MAGWriterTests: XCTestCase {
    
    var emptyDir: URL {
        return Bundle(for: MAGWriterTests.self).resourceURL!.appendingPathComponent("emptyDir")
    }
    
    var noJSONDir: URL {
        return Bundle(for: MAGWriterTests.self).resourceURL!.appendingPathComponent("dirNoJSON")
    }
    
    var validInputDir: URL {
        return Bundle(for: MAGWriterTests.self).resourceURL!.appendingPathComponent("ReaderFilesIn")
    }
    
    var outputDir: URL {
        return FileManager.default.temporaryDirectory.appendingPathComponent("magOutput")
    }
    
    override func setUp() {
        XCTAssertNoThrow(try validInputDir.checkResourceIsReachable())
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func testWriterFailsNoInputDirectory() {
        let magWriter = MAGWriter(inputPath: "nodir", outputPath: "")
        do {
            
            try magWriter.processFiles()
        } catch {
            XCTAssertNotNil(error)
            XCTAssert((error as NSError).code == NSFileReadNoSuchFileError)
        }
    }
    
    func testWriterFailsEmptyInputDirectory() {
        let magWriter = MAGWriter(inputPath: self.emptyDir.path, outputPath: "")
        do {
            try magWriter.processFiles()
        } catch {
            XCTAssertNotNil(error)
            XCTAssert((error as NSError).code == NSFileNoSuchFileError)
        }
    }
    
    func testWriterFailsNoInfoJsonFileFound() {
        let magWriter = MAGWriter(inputPath: self.noJSONDir.path, outputPath: "")
        
        do {
            try magWriter.processFiles()
        } catch {
            XCTAssertNotNil(error)
            XCTAssert((error as NSError).code == NSFileNoSuchFileError)
        }
    }
    
    func testWriterFilesCreationSuccess() {
        let magWriter = MAGWriter(inputPath: self.validInputDir.path, outputPath:self.outputDir.path)
        XCTAssertNoThrow(try magWriter.processFiles())
        XCTAssertTrue(FileManager.default.fileExists(atPath: self.outputDir.path.appending("/" + magWriter.defaultMagFileName)))
        XCTAssertTrue(FileManager.default.fileExists(atPath: self.outputDir.path.appending("/" + magWriter.defaultThumbFileName)))
        try! FileManager.default.removeItem(atPath: self.outputDir.path)
    }
    
    func testWriterFilesCreationCustomNamesSuccess() {
        let magWriter = MAGWriter(inputPath: self.validInputDir.path, outputPath:self.outputDir.path)
        magWriter.magFileName = "boatclub.mag"
        magWriter.thumbFileName = "boatclub.thumb"
        XCTAssertNoThrow(try magWriter.processFiles())
        XCTAssertTrue(FileManager.default.fileExists(atPath: self.outputDir.path.appending("/" + magWriter.magFileName!)))
        XCTAssertTrue(FileManager.default.fileExists(atPath: self.outputDir.path.appending("/" + magWriter.thumbFileName!)))
        try! FileManager.default.removeItem(atPath: self.outputDir.path)
    }
    
}
