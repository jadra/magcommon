//
//  MAGReaderTests.swift
//  MagAppTests
//
//  Created by john on 5/23/18.
//  Copyright © 2018 john. All rights reserved.
//

import XCTest
//@testable import MagApp

class MAGReaderTests: XCTestCase {
    
    var magBinariesPath: URL {
        return Bundle(for: MAGReaderTests.self).resourceURL!.appendingPathComponent("ReaderFilesOut")
    }
    
    var magazineFilePath: String {
        return magBinariesPath.appendingPathComponent("out.mag").path
    }
    
    var thumbFilePath: String {
        return magBinariesPath.appendingPathComponent("out.thumb").path
    }
    
    var thumbsExtractDir: String {
        return FileManager.default.temporaryDirectory.appendingPathComponent("thumbs").path
    }
    
    override func setUp() {
        let fileManager = FileManager.default
        do {
            try fileManager.createDirectory(atPath: thumbsExtractDir, withIntermediateDirectories: false, attributes: nil)
        }
        catch {}
        super.setUp()
    }
    
    override func tearDown() {
        let fileManager = FileManager.default
        XCTAssertNoThrow(try fileManager.removeItem(atPath: thumbsExtractDir))
        super.tearDown()
    }

    func testThumbExtractionMagazinePathFailure() {
        XCTAssertThrowsError(try MAGReader(magazinePath: "", thumbsPath: thumbFilePath))
    }
    
    func testThumbExtractionThumbFilePathFailure() {
        XCTAssertThrowsError(try MAGReader(magazinePath: magazineFilePath, thumbsPath: ""))
    }

    func testThumbExtractionSuccess() {
        let expectation = XCTestExpectation(description: "Thumb extraction succeeds")
        let magReader = try! MAGReader(magazinePath: magazineFilePath, thumbsPath: thumbFilePath)
        magReader.extractThumbs(toDirectory: thumbsExtractDir) { (progress, finished, error) in
            if finished {
                XCTAssertNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testThumbExtractionNoThumbCacheDirectoryFailure() {
        let expectation = XCTestExpectation(description: "Thumb extraction fails with no file found")
        let magReader = try! MAGReader(magazinePath: magazineFilePath, thumbsPath: thumbFilePath)
        magReader.extractThumbs(toDirectory: "") { (progress, finished, error) in
            if finished {
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testPageExtractionSuccess() {
        let expectation = XCTestExpectation(description: "Page extraction succeeds")
        let magReader = try! MAGReader(magazinePath: magazineFilePath, thumbsPath: thumbFilePath)
        magReader.extractPage(atIndex: 0) { (pageNumber, magPage) in
            XCTAssertNil(magPage.error);
            XCTAssertNotNil(magPage.imageData)
            XCTAssertNotNil(magPage.links)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testPageExtractionOutOfRangeFailure() {
        let expectation = XCTestExpectation(description: "Page extraction fails")
        let magReader = try! MAGReader(magazinePath: magazineFilePath, thumbsPath: thumbFilePath)
        magReader.extractPage(atIndex: 14) { (pageNumber, magPage) in
            XCTAssertNotNil(magPage.error);
            XCTAssertNil(magPage.imageData)
            XCTAssertNil(magPage.links)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
        
}
