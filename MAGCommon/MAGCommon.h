//
//  MAGCommon.h
//  MAGCommon
//
//  Created by john on 6/12/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for MAGCommon.
FOUNDATION_EXPORT double MAGCommonVersionNumber;

//! Project version string for MAGCommon.
FOUNDATION_EXPORT const unsigned char MAGCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MAGCommon/PublicHeader.h>
#import <MAGCommon/MAGFormat.h>
#import <MAGCommon/MAGInfo.h>
#import <MAGCommon/MAGLink.h>
#import <MAGCommon/MAGThumbnailCreator.h>
#import <MAGCommon/MAGWriter.h>
#import <MAGCommon/MAGReader.h>
#import <MAGCommon/MAGPage.h>

