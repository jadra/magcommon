//
//  MAGThumbnailCreator.m
//  MAGCommon
//
//  Created by john on 6/12/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGThumbnailCreator.h"
#import <AppKit/AppKit.h>

@implementation MAGThumbnailCreator
+ (NSData *)dataFromCIImage:(id)image {
    NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc] initWithCIImage:image];
    return [imageRep representationUsingType:NSJPEGFileType properties:@{}];
}
@end
