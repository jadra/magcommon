//
//  MAGLink.h
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>


/**
 The type of link
*/
typedef NS_ENUM(NSInteger, MAGLinkType) {
    /** Page to page link */
    MAGLinkInternal,
    /** Web link */
    MAGLinkExternal
};


/**
 A MAGLink class contains information about the placement and type of interactive links.
 Interactive links are either internal (page to page) or external (website).
 */
@interface MAGLink : NSObject

/** The string that uniquely identifies this link. */
@property (nonatomic)           NSString *identifier;

/** The type of link. */
@property (nonatomic, assign)   MAGLinkType type;

/** The dimensions of the link */
@property (nonatomic)           NSDictionary *dimensions;

/** The link (including protocol) of the link, if external. */
@property (nonatomic)           NSString *href;

/** The page to link to, if internal. */
@property (nonatomic)           NSNumber *linkTo;


/**
 Create a MAGLink object from the provided dictionary.
 @param dictionary The dictionary containing information to create an instance of MAGLink.
 @return MAGLink instance.
 */
+ (MAGLink *)linkFromDictionary:(NSDictionary *)dictionary;

/**
 Create a CGRect struct from the dimensions dictionary.
 @return CGRect struct of dimensions.
 */
- (CGRect)rectDimensions;

@end
