//
//  MAGPage.m
//  MagApp
//
//  Created by john on 5/29/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGPage.h"
#import "MAGLink.h"

@implementation MAGPage
- (instancetype)initWithPageNumber:(MAGPageNumber)pageNumber links:(NSArray<MAGLink *>* _Nullable)links imageData:(NSData *  _Nullable)imageData error:(NSError * _Nullable)error {
    self = [super init];
    if (self) {
        _pageNumber = pageNumber;
        _links = links;
        _imageData = imageData;
        _error = error;
    }
    return self;
}
@end
