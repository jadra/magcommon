//
//  MAGInfo.m
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGInfo.h"
#import "MAGLink.h"

@implementation MAGInfo

+ (MAGInfo *)infoFromDictionary:(NSDictionary *)dictionary {
    MAGInfo *info = [MAGInfo new];
    
    for (NSString *key in [dictionary keyEnumerator]) {
        if ([key isEqualToString:@"version"]) {
            info.version = dictionary[@"version"];
        }
        else if ([key isEqualToString:@"pageCount"]) {
            info.pageCount = dictionary[@"pageCount"];
        }
        else if ([key isEqualToString:@"width"]) {
            info.width = dictionary[@"width"];
        }
        else if ([key isEqualToString:@"height"]) {
            info.height = dictionary[@"height"];
        }
        else if ([key isEqualToString:@"name"]) {
            info.name = dictionary[@"name"];
        }
        else if ([key isEqualToString:@"issue"]) {
            info.issue = dictionary[@"issue"];
        }
        else if ([key isEqualToString:@"publisher"]) {
            info.publisher = dictionary[@"publisher"];
        }
        else if ([key isEqualToString:@"links"]) {
            info.links = @{@"links" : dictionary[@"links"]};
        }
    }
    
    return info;
}
@end
