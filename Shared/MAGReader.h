//
//  MAGReader.h
//  MagApp
//
//  Created by john on 5/23/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import "MAGPage.h"


NSString * _Nonnull const MAG_READER_DOMAIN = @"com.jomagam.magapp";

typedef float MAGExtractionProgress;

typedef BOOL MAGExtractionCompleted;

/** A block detailing information about thumbnail extraction. */
typedef void(^ThumbExtractionInfo)(MAGExtractionProgress, MAGExtractionCompleted, NSError * __nullable);

/** A block with information about a page extraction. */
typedef void(^MAGPageExtractionInfo)(MAGPageNumber, MAGPage * __nonnull);



/**
 The MagReader object is responsible for reading a file of mag format, extracting it's thumbnails and extracting a page on demand.
 */
@interface MAGReader : NSObject

/** The maximum page number of this mag file. */
@property (nonatomic, readonly) NSUInteger maxPage;

/** The original size of the images contained in the mag file. */
@property (nonatomic, readonly) CGSize originalSize;


/**
 Creates a mag file format reader

 @param magazinePath The path of the mag file.
 @param thumbsFilePath The path of the thumb file.
 @param error The error object to fill.
 @return A Magreader instance or null if there was an error.
 */
- (__nullable instancetype)initWithMagazinePath:(NSString * __nonnull)magazinePath thumbsPath:(NSString * __nonnull)thumbsFilePath error:(NSError * __nullable * __nullable)error;


/**
 Extracts thumbnails of mag file images

 @param thumbsDirectory Directory to extract thumbnails to.
 @param extractionInfo Information about the progress, completion and failure.
 */
- (void)extractThumbsToDirectory:(NSString * __nonnull)thumbsDirectory thumbExtractionInfo:(ThumbExtractionInfo __nullable)extractionInfo;


/**
 Extracts an image (page).

 @param index The index of the image to extract.
 @param result The result of the extraction.
 */
- (void)extractPageAtIndex:(NSNumber * __nonnull)index result:(MAGPageExtractionInfo __nullable)result;

@end
