//
//  MAGFormat.h
//  ReaderMake
//
//  Created by john on 5/20/18.
//  Copyright © 2018 john. All rights reserved.
//

#ifndef MAGFormat_h
#define MAGFormat_h

/** Size of the metadata in the file. */
#define MAGFORMAT_METADATA_SIZE    832
/** Size of the picture offset table. */
#define MAGFORMAT_JUMPTABLE_SIZE  1600
/** Total of the previous two sizes. */
#define MAGFORMAT_HEADER_SIZE     2432
/** Size of the name, publisher and issue strings. */
#define MAGFORMAT_CHAR_ARR_SIZE    256
/** Maximum number of entries in the offset table. */
#define MAGFORMAT_JUMPTABLE_COUNT  100


/**
 The MAGPicture struct contains the offset of an image in a mag file as well as it's size.
 */
typedef struct {
    /** Offset of picture in file. */
    unsigned long offset;
    /** Size of the picture in file. */
    unsigned long size;
} MAGPicture;

/**
 The MAGFormatHeader contains information about the mag file. It is also
 used to create the mag file itself.
 */
typedef struct {
    /** The version of the MAGWriter */
    long version;
    /** The total pages in the file, counted from 1. */
    long pageCount;
    /** The width of the images. */
    long width;
    /** The height of the images. */
    long height;
    /** Offset of the links section of the file. */
    long linksOffset;
    /** The size of the links section of the file. */
    long linksSize;
    /**  Offset of the images section of the file. */
    long picsOffset;
    /** The size of the images section of the file. */
    long picsSize;
    /** Name of the magazine. */
    char name[MAGFORMAT_CHAR_ARR_SIZE];
    /** Issue of the magazine. */
    char issue[MAGFORMAT_CHAR_ARR_SIZE];
    /** Publisher of the magazine. */
    char publisher[MAGFORMAT_CHAR_ARR_SIZE];
    /** Table of offsets to images in the file.*/
    MAGPicture picJumpTable[MAGFORMAT_JUMPTABLE_COUNT];
} MAGFormatHeader;


/**
 Contains information about the header section of the thumb file.
 */
typedef struct {
    /** Count of pages in the thumb file. */
    long pageCount;
    /** Offset, size table of thumb file. */
    MAGPicture thumbJumpTable[MAGFORMAT_JUMPTABLE_COUNT];
} MAGThumbFormatHeader;

#endif /* MAGFormat_h */
