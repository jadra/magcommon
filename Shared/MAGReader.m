//
//  MAGReader.m
//  MagApp
//
//  Created by john on 5/23/18.
//  Copyright © 2018 john. All rights reserved.
//

#import "MAGReader.h"
#import "MAGFormat.h"
#import "MAGLink.h"
#include <sys/mman.h>

const char * MAG_QUEUE_NAME = "com.jomagam.magapp.queue";
NSString * const MAG_THUMBFILE_FILENAME_FORMAT = @"%d.jpg";
NSInteger const MAG_SEMAPHORE_RESOURCES = 2;

void pageExtractionQueueCleanup(void *);

typedef struct {
    void *map;
    unsigned long long size;
} MAGMapContext;

@interface MAGReader()
@property (nonnull, nonatomic) NSString *magazinePath;
@property (nonnull, nonatomic) NSString *thumbsFilePath;
@property (nonatomic) void *magFileMap;
@property (nonatomic, assign) unsigned long long magFileSize;
@property (nonatomic, assign) MAGFormatHeader *magFileHeader;
@property (nonatomic) NSDictionary *magLinks;
@property (nonatomic) NSNumberFormatter *numberFormatter;
@property (nonatomic) NSOperationQueue *thumbExtractionQueue;
@property (nonatomic) dispatch_semaphore_t mapSemaphore;
@property (nonatomic) dispatch_queue_t pageExtractionQueue;
@end

@implementation MAGReader

- (__nullable instancetype)initWithMagazinePath:(NSString * __nonnull)magazinePath thumbsPath:(NSString * __nonnull)thumbsFilePath error:(NSError * __nullable * __nullable)error
{
    self = [super init];
    if (self) {
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        if (![fileManager fileExistsAtPath:magazinePath]) {
            NSString *description = [NSString stringWithFormat:@"Magazine file at %@ does not exist.", magazinePath];
            *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError userInfo:@{NSLocalizedDescriptionKey : description}];
        }
        else if (![fileManager fileExistsAtPath:thumbsFilePath]) {
            NSString *description = [NSString stringWithFormat:@"Thumbs file at %@ does not exist.", thumbsFilePath];
            *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError userInfo:@{NSLocalizedDescriptionKey : description}];
        }
        
        if (*error) {
            return nil;
        }
        
        _magFileSize = 0;
        _magazinePath = magazinePath;
        _thumbsFilePath = thumbsFilePath;
        
        [self configureMemoryMapping:error];
        if (*error) {
            return nil;
        }
        
        _numberFormatter = [NSNumberFormatter new];
        
        _mapSemaphore = dispatch_semaphore_create(MAG_SEMAPHORE_RESOURCES);
        _pageExtractionQueue = dispatch_queue_create(MAG_QUEUE_NAME, NULL);
        MAGMapContext *context = (MAGMapContext *)malloc(sizeof(MAGMapContext));
        context->map = _magFileMap;
        context->size = _magFileSize;
        dispatch_set_context(_pageExtractionQueue, context);
        dispatch_set_finalizer_f(_pageExtractionQueue, &pageExtractionQueueCleanup);
    }
    return self;
}

void pageExtractionQueueCleanup(void *mapContext) {
    MAGMapContext *context = (MAGMapContext *)mapContext;
    if (context->map != NULL) {
        if (munmap(context->map, context->size) == -1) {
            perror("munmap for magfile failed!");
        }
    }
    free(context);
}

- (void)configureMemoryMapping:(NSError **)error {
    NSString *errorDescription;
    NSDictionary *fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:_magazinePath error:nil];
    _magFileSize = [fileAttribs[NSFileSize] unsignedLongLongValue];

    NSFileHandle *magFileHandle = [NSFileHandle fileHandleForReadingAtPath:_magazinePath];
    if (!magFileHandle) {
        errorDescription = [NSString stringWithFormat:@"File: %@ could not be read.", _magazinePath];
        *error = [NSError errorWithDomain:NSCocoaErrorDomain
                                     code:NSFileReadNoSuchFileError
                                 userInfo:@{NSLocalizedDescriptionKey : errorDescription}];
        return;
    }
    
    int magFileDescriptor = [magFileHandle fileDescriptor];
    if (magFileDescriptor == -1) {
        errorDescription = [NSString stringWithFormat:@"Invalid file descriptor for file: %@",_magazinePath];
        *error = [NSError errorWithDomain:MAG_READER_DOMAIN code:3 userInfo:@{NSLocalizedDescriptionKey : errorDescription}];
        [magFileHandle closeFile];
        return;
    }
    
    _magFileMap = mmap(0, _magFileSize, PROT_READ, MAP_PRIVATE, magFileDescriptor, 0);
    
    if (_magFileMap == MAP_FAILED) {
        errorDescription = [NSString stringWithFormat:@"Memory mapping failed for file: %@",_magazinePath];
        *error = [NSError errorWithDomain:MAG_READER_DOMAIN code:4 userInfo:@{NSLocalizedDescriptionKey : errorDescription}];
        return;
    }
    
    if (magFileHandle) {
        [magFileHandle closeFile];
    }
    
    _magFileHeader = (MAGFormatHeader *)&_magFileMap[0];
    NSData *linksData = [NSData dataWithBytes:&_magFileMap[_magFileHeader->linksOffset] length:_magFileHeader->linksSize];
    _magLinks = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:linksData options:NSJSONReadingAllowFragments error:error];
}

- (NSUInteger)maxPage {
    return _magFileHeader->pageCount - 1;
}

- (CGSize)originalSize {
    return CGSizeMake((CGFloat)_magFileHeader->width, (CGFloat)_magFileHeader->height);
}

- (void)extractThumbsToDirectory:(NSString * __nonnull)thumbsDirectory thumbExtractionInfo:(ThumbExtractionInfo __nullable)extractionInfo {
    
    BOOL isDirectory = YES;
    BOOL directoryExists = [[NSFileManager defaultManager] fileExistsAtPath:thumbsDirectory isDirectory:&isDirectory];
    
    if (!directoryExists || !isDirectory) {
        if (extractionInfo) {
            NSString *errorDescription = [NSString stringWithFormat:@"%@ does not exist.", thumbsDirectory];
            NSError *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileReadNoSuchFileError userInfo:@{ NSLocalizedDescriptionKey : errorDescription}];
            extractionInfo(0, YES,error);
            return;
        }
    }
    
    if (!_thumbExtractionQueue) {
        _thumbExtractionQueue = [NSOperationQueue new];
    }
    
    [_thumbExtractionQueue addOperationWithBlock:^{
        NSFileManager *fileManager = [NSFileManager new];
        NSFileHandle *thumbFileHandle;
        NSError *extractionError;
        
        @try {
                thumbFileHandle = [NSFileHandle fileHandleForReadingAtPath:self.thumbsFilePath];
                MAGThumbFormatHeader header;
                NSData *headerData = [thumbFileHandle readDataOfLength:sizeof(header)];
                [headerData getBytes:&header length:sizeof(MAGThumbFormatHeader)];
                
                for (int i = 0; i < header.pageCount; i++) {
                    NSString *thumbFileName = [thumbsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:MAG_THUMBFILE_FILENAME_FORMAT, i]];
                    if (![fileManager fileExistsAtPath:thumbFileName]) {
                        NSData *picData = [thumbFileHandle readDataOfLength:header.thumbJumpTable[i].size];
                        [picData writeToFile:thumbFileName atomically:NO];
                    }
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        if (extractionInfo) {
                            extractionInfo((float) i / (float)header.pageCount, NO, nil);
                        }
                    }];
                }
        }
        @catch (NSException *exception) {
            extractionError = [NSError errorWithDomain:MAG_READER_DOMAIN code:2 userInfo:exception.userInfo];
        }
        @finally {
            if (thumbFileHandle) {
                [thumbFileHandle closeFile];
            }
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                if (extractionInfo) {
                    extractionInfo(1., YES, extractionError);
                }
            }];
        }
    }];
}

- (void)cancelThumbExtraction {
    if (_thumbExtractionQueue) {
        [_thumbExtractionQueue cancelAllOperations];
    }
}

- (void)extractPageAtIndex:(NSNumber * __nonnull)index result:(MAGPageExtractionInfo __nullable)result {
    
    MAGPageNumber pageNumber = [index unsignedIntegerValue];
    if (pageNumber > (_magFileHeader->pageCount - 1)) {
        if (result) {
            NSString *errorDescription = [NSString stringWithFormat:@"Provided page number: %ld exceeds pages in magazine.", pageNumber];
            NSError *error = [NSError errorWithDomain:MAG_READER_DOMAIN code:5 userInfo:@{NSLocalizedDescriptionKey : errorDescription}];
            result(pageNumber, [[MAGPage alloc]initWithPageNumber:pageNumber links:nil imageData:nil error:error]);
            return;
        }
    }
    __block NSError *error;
    __block __weak MAGReader *weakSelf = self;
    
    dispatch_async(_pageExtractionQueue, ^{
        dispatch_semaphore_wait(weakSelf.mapSemaphore, DISPATCH_TIME_FOREVER);
        
        NSArray *links = [weakSelf fetchLinks:index];
        MAGPicture picture = weakSelf.magFileHeader->picJumpTable[pageNumber];
        NSData *imageData = [NSData dataWithBytes:&weakSelf.magFileMap[picture.offset] length:picture.size];
        if (!imageData) {
            error = [NSError errorWithDomain:MAG_READER_DOMAIN code:6 userInfo:@{NSLocalizedDescriptionKey : @"Couldn't extract image."}];
        }
        
        dispatch_semaphore_signal(weakSelf.mapSemaphore);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (result) {
                MAGPage *magPage = [[MAGPage alloc] initWithPageNumber:pageNumber links:links imageData:imageData error:error];
                result(pageNumber, magPage);
            }
        });
    });
}

- (NSArray *)fetchLinks:(NSNumber *)pageNumber {
    if (_magLinks) {
        NSMutableArray *links = [NSMutableArray array];
        NSArray *linkDicts = _magLinks[@"links"][[_numberFormatter stringFromNumber:pageNumber]];
        for (NSDictionary *linkDict in linkDicts) {
            MAGLink *link = [MAGLink linkFromDictionary:linkDict];
            [links addObject:link];
        }
        return links;
    }
    return nil;
}

@end
