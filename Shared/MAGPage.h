//
//  MAGPage.h
//  MagApp
//
//  Created by john on 5/29/18.
//  Copyright © 2018 john. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MAGLink;

typedef NSUInteger MAGPageNumber;


/**
 The MAGPage class contains information about a page in a magfile.
 A page is conceptually an image and link information. An instance of this class is created by a MAGReader when a page is extracted.
 */
@interface MAGPage : NSObject

/** The page number of this page. */
@property (nonatomic, assign) MAGPageNumber pageNumber;

/** An array of MAGLink objects that this page contains, if any. */
@property (nullable, nonatomic) NSArray<MAGLink *> *links;

/** The image belonging to this page. While this property is nullable, it is considered an error if it is. */
@property (nullable, nonatomic) NSData *imageData; //UIImage *image;

/** An error object if there was a problem during extraction of the page. */
@property (nullable, nonatomic) NSError *error;


/**
 Creates a MAGPage object.
 @param pageNumber The number of the page.
 @param links An array of links.
 @param image The image
 @param error An error if any.
 @return A MAGPage instance.
 */
- ( _Nonnull instancetype)initWithPageNumber:(MAGPageNumber)pageNumber links:(NSArray<MAGLink *>* _Nullable)links imageData:(NSData * _Nullable)image error:(NSError * _Nullable)error;
@end
